package com.gitlab.edufuga.bookwards.questioner

import com.gitlab.edufuga.bookwards.messageprinter.Colors
import com.gitlab.edufuga.bookwards.messageprinter.MessagePrinter
import com.gitlab.edufuga.bookwards.shuffler.SentenceShuffler
import com.gitlab.edufuga.wordlines.core.Status
import com.gitlab.edufuga.wordlines.core.WordStatusDate
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader
import com.gitlab.edufuga.wordlines.recordwriter.RecordWriter
import com.gitlab.edufuga.wordlines.sentenceparser.SentenceParser
import com.gitlab.edufuga.wordlines.sentencereader.SentenceReader
import com.gitlab.edufuga.wordlines.sentencesreader.SentencesReader
import groovy.transform.CompileStatic
import org.jline.terminal.Terminal
import org.jline.terminal.TerminalBuilder

import java.awt.Color
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.function.Predicate
import java.util.function.Supplier
import java.util.stream.Collectors
import java.util.stream.Stream

@CompileStatic
class Questioner {
    private String statusFolder
    private SentencesReader sentencesReader
    private FileSystemRecordReader recordReader
    private RecordWriter recordWriter
    private SentenceParser sentenceParser
    private MessagePrinter messagePrinter
    private SentenceShuffler sentenceShuffler

    private Path wordsPath
    private Terminal terminal

    private boolean jump = false
    private boolean hide = false

    Questioner (Path wordsPath, SentencesReader sentencesReader, String statusFolder, Path sentencesFolder) {
        this.terminal = TerminalBuilder.builder().system(true).jansi(true).build()
        terminal.enterRawMode()

        this.wordsPath = wordsPath
        this.sentencesReader = sentencesReader
        this.statusFolder = statusFolder
        this.recordReader = new FileSystemRecordReader (statusFolder, sentencesFolder)
        this.recordWriter = new RecordWriter (statusFolder, recordReader)
        this.sentenceParser = new SentenceParser(recordReader)

        this.messagePrinter = new MessagePrinter()
        this.sentenceShuffler = new SentenceShuffler(sentenceParser, messagePrinter)
    }

    // Question words, by default excluding a word if it is already present in the status folder.
    void questionWords(Predicate<String> wordPredicate={String word -> !doesWorkHaveToBeExcluded(word)}) {
        boolean splitTsv = (wordsPath.endsWith("metadata.tsv"))

        try(Stream<String> words = Files.lines(wordsPath) ) {
            words.forEachOrdered() { String word ->
                String frequency = ""
                if (splitTsv) {
                    def strings = word.split ("\t")
                    (word, frequency) = [strings [0], strings [1]]
                }
                if (wordPredicate(word)) {
                    if (jump) {
                        jump = !jump
                    }
                    else {
                        question(word, frequency)
                    }
                }
            }
        }
        catch (Exception ignored)
        {
            println "Closing..."
        }
        finally {
            terminal.close()
        }
    }

    void reviewWords(Status status) {
        List<String> allLines = Files.readAllLines(wordsPath)

        List<String> allWords = new ArrayList<>()
        Map<String, String> wordToFrequency = new HashMap<>()

        Set<String> reviewedWords = new HashSet<>()

        boolean splitTsv = (wordsPath.endsWith("metadata.tsv"))
        for (String line : allLines) {
            if (splitTsv) {
                def strings = line.split("\t")
                String word = strings[0]
                String frequency = strings[1]
                wordToFrequency.put(word, frequency)
                allWords.add(word)
            }
            else {
                wordToFrequency.put(line, "?")
                allWords.add(line)
            }
        }

        List<WordStatusDate> allRecords = new ArrayList<>()

        for (String word : allWords) {
            WordStatusDate record = recordReader.readRecord(word)
            allRecords.add(record)
        }

        List<WordStatusDate> allRecordsWithStatus = allRecords.stream()
                .filter(record -> status.equals(record.getStatus()))
                .sorted((r1, r2) -> r1.getDate().toLocalDate().compareTo(r2.getDate().toLocalDate()))
                .collect(Collectors.toList())
        if (allRecordsWithStatus == null || allRecordsWithStatus.size() == 0)
        {
            throw new RuntimeException("No records to review!")
        }

        List<String> words = allRecordsWithStatus.stream().map(WordStatusDate::getWord).collect(Collectors.toList())
        if (words == null || words.size() == 0)
        {
            throw new RuntimeException("No words to review!")
        }
        for (String word : words) {
            if (jump) {
                jump = !jump
            }
            else {
                if (reviewedWords.contains(word.toLowerCase())) {
                    continue
                }
                question(word, wordToFrequency.get(word) ?: "?")
                reviewedWords.add(word.toLowerCase())
            }
        }

        terminal.close()
    }

    boolean doesWorkHaveToBeExcluded(String word) {
        return wordIsPresentInStatusFolder(word) || wordHasCorrespondingRecord(word)
    }

    boolean wordIsPresentInStatusFolder(String word) {
        Files.exists(Paths.get(statusFolder, "${word}.tsv"))
                || Files.exists(Paths.get(statusFolder, "${word.toLowerCase()}.tsv"))
    }

    boolean wordHasCorrespondingRecord(String word) {
        WordStatusDate record = recordReader.readRecord(word)
        return record != null && record.status != null
    }

    void question(String word, String frequency) {
        WordStatusDate result = askStatusAtCurrentDate(word, frequency)
        if (!jump) {
            recordWriter.write(result)
        }
    }

    private WordStatusDate askStatusAtCurrentDate(String word, String frequency="") {
        SentenceReader sentenceReader = new SentenceReader(sentencesReader) // The sentencereader is local to a word.
        Status status = Status.UNKNOWN
        boolean  statusFound = false
        String hiddenWord = "?" * word.size()
        int hiddenWordIndex = 0

        while (!statusFound && !jump) {
            try {
                messagePrinter.printMessage (hide ? hiddenWord : word, Colors.noopt, true, frequency)
                String sentence = sentenceReader.getSentenceForWord(word)
                List<WordStatusDate> wordsInSentence = sentenceParser.parseSentenceIntoRecords (sentence)
                Supplier<Map<Integer, Boolean>> charactersToUnderlineSupplier
                        = () -> messagePrinter.getCharactersToUnderline (sentence, word)
                messagePrinter.printMessage (sentence, wordsInSentence, charactersToUnderlineSupplier, true, true, hide)

                // Guess the hidden word by typing it.
                boolean guessed = false
                if (hide) {
                    messagePrinter.printMessage("\nTry to guess the word!\n")
                    String enteredWord = ""
                    while (!guessed) {
                        Character c = Character.valueOf((char) terminal.reader().read())
                        enteredWord += c.toString()
                        messagePrinter.printMessage(c.toString(), Colors.noopt, false)

                        if (word == enteredWord) {
                            messagePrinter.printMessage("\nBingo!", Color.MAGENTA)
                            guessed = true
                            break
                        }

                        if ((enteredWord?.size() == word.size() && word != enteredWord) || enteredWord?.size() > word.size())
                        {
                            messagePrinter.printMessage("\nNope. Let's try again.\n")
                            enteredWord = ""
                            guessed = false
                            continue
                        }

                        if (enteredWord.contains("\n")) {
                            guessed = false
                            break
                        }

                        if (enteredWord == "?")
                        {
                            messagePrinter.printMessage("\n")
                            guessed = false
                            hiddenWord = word[0..hiddenWordIndex] + "?" * (word.size() - hiddenWordIndex - 1)
                            hiddenWordIndex++
                            break
                        }
                    }
                }

                if (!hide) {
                    guessed = true; // HACK!
                }

                if (guessed) {
                    messagePrinter.printMessage("\n")
                    messagePrinter.printMessage("Possibilities: ${Status.values().join(", ")}\n")
                    Character c = Character.valueOf((char) terminal.reader().read())

                    if (c == "q") {
                        throw new Exception("Interrupted.")
                    }
                    if (c == "j") {
                        messagePrinter.print("\nJumping to next word.")
                        jump = true
                    }
                    if (c == "h") {
                        hide = !hide
                    }

                    if (!jump) {
                        status = Status.getFor(c)
                        Objects.requireNonNull(status)
                        statusFound = true
                        messagePrinter.printMessage("You entered status '$status'.")
                    }
                }
            }
            catch (Exception ignored) {
                statusFound = false
            }
        }
        messagePrinter.printMessage ("\n")

        Date date = new Date()
        return new WordStatusDate(word, status, date)
    }

    static void main(String[] args) {
        if(args.size()!=3) {
            println "Give me the words file, the status folder and the sentences folder."
            return
        }

        def wordsFile = new File(args[0])
        if (!wordsFile.exists()) {
            println "The given file does not exist."
            return
        }
        println "Working with words file '$wordsFile.absolutePath'."
        Path wordsPath = wordsFile.toPath()

        def statusFolder = args[1]
        println "Results will be saved in status folder '$statusFolder'."

        def sentencesFolder = args[2]
        SentencesReader sentencesReader = new SentencesReader(new File(sentencesFolder))

        println "Going to review COMPREHENDED words."
        def questioner = new Questioner(wordsPath, sentencesReader, statusFolder, Paths.get(sentencesFolder))
        questioner.reviewWords(Status.COMPREHENDED)
    }
}
